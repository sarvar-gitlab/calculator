function numberClick(num){
  document.getElementById("myInput").value += num;
}

function plusClick(){
  document.getElementById("myInput").value += "+";
}

function multipleClick(){
  document.getElementById("myInput").value += "*";
}

function minusClick(){
  document.getElementById("myInput").value += "-";
}

function clearClick(){
  document.getElementById("myInput").value = "";
}

function dotClick(){
  document.getElementById("myInput").value += ".";
}

function divisionClick(){
  document.getElementById("myInput").value += "/";
}

function equalClick(){
  let str = document.getElementById("myInput").value;
  // console.log(str);
  let strNew, isHP, isHMi, isHMu,isHD;
  for(let i = 0; i<str.length; i++){
    if(str[i] == "+"){
      isHP = true;
    }else if(str[i] == "-"){
      isHMi = true;
    }else if(str[i] == "*"){
      isHMu = true;
    }else if(str[i] == "/"){
      isHD = true;
    }
  }
  if(isHP){
    strNew = str.split("+");
    document.getElementById("myInput").value =  parseInt(strNew[0], 10)+parseInt(strNew[1], 10);
  }else if(isHMi){
    strNew = str.split("-");
    document.getElementById("myInput").value =  parseInt(strNew[0], 10)-parseInt(strNew[1], 10);
  }else if(isHMu){
    strNew = str.split("*");
    document.getElementById("myInput").value =  parseInt(strNew[0], 10)*parseInt(strNew[1], 10);
  }else if(isHD){
    strNew = str.split("/");
    document.getElementById("myInput").value =  parseInt(strNew[0], 10)/parseInt(strNew[1], 10);
  }
  
}

